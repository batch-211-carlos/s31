//NODE JS INTRODUCTION

//Use the 'require' derective to load Node.js modules
//A 'module' is a software component or part of a program that contains one or more routines
//The 'http module' lets Node.js transfer data using the Hyper Text Transfer Protocol
//The 'http module' is a set of individual files that contain code to create a 'component' that helps establish data transfer between applications
//HTTP is a protocol that allow the fetching of resources such as HTML documents
//Clients (browsers) and servers (Node.js/Express.JS applications) communicate by exchanging individiual messages 
//The messages sent by the client, usually, a Web Browser, are called requests
//The messages sent by the server as an answer are called responses

let http = require("http");

//The http module has a create server method that accepts the function as an argument and allows for a creation of a server
//The arguments passed in the function are request and response objects that contains methods that allows us to receive requests from the client and send responses back to it
http.createServer(function (request, response){

/*
	Use the 'writeHead()' method to:
	- Set a status code for the response- a 200 means OK
	- Set the content-type of the response as a plain text message
*/
	response.writeHead(200, {'Content-Type': 'text/plain'})

	//Send the response with a text content "Hello World"
	response.end('Hello World');

}).listen(4000)

//A port is a virtual point where network connections start and end 
//Each port is associated with specific process or service
//the server will be assigned to port e.g. '4000' via the 'listen(4000)' method where the server will listen to any requests that are sent to it, eventually communicating with our server

console.log('Server running at localhost:4000');
